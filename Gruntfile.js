module.exports = function(grunt){


// Project configuration.
    grunt.initConfig({

        browserSync: {
            bsFiles: {
                src : 'built/css/style.css'

            }

        },

        sass: {                              // Task
            dist: {                            // Target
                files: {                         // Dictionary of files
                    'built/css/style.css': 'scss/style.scss'     // 'destination': 'source'

                }
            }
        },

        concat: {
            js: {
                src: ['js/game.js', 'js/parallax.js'],
                dest: 'built/js/script.js',
            },

            // css: {
            //     src:['css/main.css', 'css/theme.css'],
            //     dest: 'built/css/style.css',
            // },
        },

        watch: {
            js: {
                files: 'js/**/*.js',
                tasks: ['concat:js'],
            },
            css: {
                files: 'css/**/*.css',
                tasks: ['concat:css'],
            },

            sass: {
                files: 'scss/style.scss',
                tasks: ['sass']
            },



        },



    }),


    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('default' ,['concat','watch', 'sass']);
};
